import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.layout.VBox
import javafx.stage.FileChooser
import kotlinx.coroutines.*
import tornadofx.View
import java.io.File
import java.lang.Exception

class MainView : View() {
    override val root: VBox by fxml("/main.fxml")

    @FXML
    lateinit var survivePathButton: Button
    @FXML
    lateinit var surviveResultView: Label
    @FXML
    lateinit var survivePathView: Label

    @FXML
    lateinit var topicPathButton: Button
    @FXML
    lateinit var topicResultView: Label
    @FXML
    lateinit var topicPathView: Label
    @FXML
    lateinit var topicIdView: TextField
    @FXML
    lateinit var topicImageUrlView: TextField
    @FXML
    lateinit var topicNameView: TextField
    @FXML
    lateinit var topicDescriptionView: TextArea


    init {
        survivePathButton.setOnMouseClicked {
            onSelectSurviveFile(FileChooser().showOpenDialog(null))
        }
        survivePathView.text = "No file selected"

        topicPathButton.setOnMouseClicked {
            onSelectTopicFile(FileChooser().showOpenDialog(null))
        }
        topicPathView.text = "No file selected"

    }

    private fun onSelectSurviveFile(file: File?) {
        survivePathView.text = file?.path ?: "No file selected"
        if (file != null) {
            surviveResultView.text = "Processing File"
            processSurviveQuestionsFile(file)
        } else {
            surviveResultView.text = "Missing File"
        }
    }

    private fun onSelectTopicFile(file: File?) {
        topicPathView.text = file?.path ?: "No file selected"
        if (file != null) {
            topicResultView.text = "Processing File"
            processTopicQuestionsFile(file)
        } else {
            topicResultView.text = "Missing File"
        }
    }

    private suspend fun drawSurviveProcessResult(isSuccess: Boolean, text: String?) {
        drawResult(surviveResultView, isSuccess, text)
    }

    private suspend fun drawTopicProcessResult(isSuccess: Boolean, text: String?) {
        drawResult(topicResultView, isSuccess, text)
    }

    private fun drawUIResult(label: Label, isSuccess: Boolean, text: String?) {
        var result = if (isSuccess) "Success" else "Error"
        if (text != null) {
            result += ", $text"
        }
        label.text = result
    }

    private suspend fun drawResult(label: Label, isSuccess: Boolean, text: String?){
        withContext(Dispatchers.Main) {
            drawUIResult(label, isSuccess, text)
        }
    }

    private fun processSurviveQuestionsFile(file: File) {
        CoroutineScope(Dispatchers.IO).launch {
           val list = QuestionFileProcessor.parseSurviveQuestions(file)
            if (list.size == 0) {
                drawSurviveProcessResult(false, "Missing text data")
            } else {
                val resultFile = File(file.parentFile, "result_${file.name}")
                var text = "["
                list.forEach {
                    if (it == list[list.size - 1]) {
                        text += "\n$it"
                    } else {
                        text += "\n$it,"
                    }
                }
                text += "]"
                resultFile.writeText(text)
                drawSurviveProcessResult(true, "saved ${list.size} questions")
            }
        }
    }

    private fun processTopicQuestionsFile(file: File) {
        val idText = topicIdView.text?:""
        if(idText.isBlank()){
            drawUIResult(topicResultView, false, "Id required")
            return
        }
        val id: Int
        try {
            id = idText.toInt()
        } catch (ignore: Exception) {
            drawUIResult(topicResultView, false, "Id must be number")
            return
        }
        val imageUrl = topicImageUrlView.text ?: ""
        val name = topicNameView.text ?: ""
        if (name.isBlank()) {
            drawUIResult(topicResultView, false, "Name required")
            return
        }
        val description = topicDescriptionView.text ?: ""
        if (description.isBlank()) {
            drawUIResult(topicResultView, false, "Description required")
            return
        }

        CoroutineScope(Dispatchers.IO).launch {
            val list = QuestionFileProcessor.parseTopicQuestions(file)
            if (list.size == 0) {
                drawTopicProcessResult(false, "Missing text data")
            } else {
                val resultFile = File(file.parentFile, "result_${file.name}")
                var text = "["
                list.forEach {
                    if (it == list[list.size - 1]) {
                        text += "\n$it"
                    } else {
                        text += "\n$it,"
                    }
                }
                text += "]"
                val stageJson = QuestionFileProcessor.buildTopicStage(text, description.trim(), imageUrl.trim(), id, name.trim())
                resultFile.writeText(stageJson)
                drawTopicProcessResult(true, "Saved ${list.size} questions for stage")
            }
        }
    }

}