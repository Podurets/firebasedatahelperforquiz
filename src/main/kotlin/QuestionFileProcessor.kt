import java.io.BufferedReader
import java.io.File
import java.lang.RuntimeException

object QuestionFileProcessor {

    private fun obtainJsonQuestion(isTrue: Boolean, question: String): String {
        return " {\n" +
                "              \"text\": \"$question\",\n" +
                "              \"isTrue\": ${if (isTrue) "true" else "false"}" +
                "            }"
    }

    fun parseSurviveQuestions(file: File): ArrayList<String> {
        val bufferedReader: BufferedReader = file.bufferedReader(Charsets.UTF_8)
        val list: ArrayList<String> = ArrayList()
        bufferedReader.useLines { lines ->
            lines.forEach {
                when {
                    it.endsWith("+") -> list.add(obtainJsonQuestion(true, it.substring(0, it.lastIndexOf("+")).trim()))
                    it.endsWith("-") -> list.add(obtainJsonQuestion(false, it.substring(0, it.lastIndexOf("-")).trim()))
                    else -> throw RuntimeException("Invalid question info:\n$it")
                }
            }
        }
        try {
            bufferedReader.close()
        } catch (ignore: Exception) {

        }
        return list
    }

    fun parseTopicQuestions(file: File): ArrayList<String> {
        val bufferedReader: BufferedReader = file.bufferedReader(Charsets.UTF_8)
        val list: ArrayList<String> = ArrayList()
        bufferedReader.useLines { lines ->
            lines.forEach {
                when {
                    it.endsWith("+") -> list.add(obtainJsonQuestion(true, it.substring(0, it.lastIndexOf("+")).trim()))
                    it.endsWith("-") -> list.add(obtainJsonQuestion(false, it.substring(0, it.lastIndexOf("-")).trim()))
                    else -> throw RuntimeException("Invalid question info:\n$it")
                }
            }
        }
        try {
            bufferedReader.close()
        } catch (ignore: Exception) {

        }
        return list
    }

    fun buildTopicStage(questionsJson: String, description: String, imageUrl: String, id: Int, name: String): String =
        "{\n" +
                "\"asks\" : $questionsJson," +
                "\n\"desc\" : \"$description\", \n" +
                "\"img\" : \"$imageUrl\",\n" +
                "\"name\" : \"$name\",\n" +
                "\"id\" : $id" +
                "\n}"

}