import javafx.stage.Stage
import tornadofx.App
import tornadofx.launch

class AppMain : App() {
    override val primaryView = MainView::class

    override fun start(stage: Stage) {
        stage.isResizable = false
        super.start(stage)
    }

}

 fun main(args: Array<String>) {
      launch<AppMain>(args)
  }